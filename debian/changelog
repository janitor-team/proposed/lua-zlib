lua-zlib (1.2-3) unstable; urgency=medium

  * Team-maintain the package (Closes: #995537).
  * Fix the debian/watch uscan control file.
  * Bump the debhelper compatibility level to 13.
  * Bump the standards version to 4.6.2.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 24 Jan 2023 16:07:11 +0300

lua-zlib (1.2-2) unstable; urgency=medium

  * Add Lua 5.4 to the list of versions to build the package.
  * Switch build dependency on debhelper to the debhelper-compat virtual
    package.
  * Bump the standards version to 4.5.0.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 08 Jul 2020 09:37:03 +0300

lua-zlib (1.2-1) unstable; urgency=medium

  * New upstream release.
  * Ignore the .gitattributes file from the upstream tarball because it breaks
    git-buildpackage.
  * Acknowledge the NMU by Aurelien Jarno.
  * Add Lua 5.3 to the list of versions to build the package.
  * Fix the debian/watch uscan control file.
  * Add myself to the uploaders list.
  * Bump the debhelper compatibility level to 12.
  * Bump the standards version to 4.4.0.
  * Fix the VCS headers in debian/control.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 31 Jul 2019 14:42:55 +0300

lua-zlib (0.2+git+1+9622739-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: Drop explicit Pre-Depends on multiarch-support
    (Closes: #870557).

 -- Aurelien Jarno <aurel32@debian.org>  Sat, 20 Jan 2018 22:12:35 +0100

lua-zlib (0.2+git+1+9622739-2) unstable; urgency=medium

  * Rebuild against Lua 5.1.5-7 (Closes: #723493)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 30 Aug 2014 21:54:50 +0200

lua-zlib (0.2+git+1+9622739-1) unstable; urgency=low

  * add Privides: ${lua:Provides} to control 
  * new upstream snapshot integrating debian patches:
    - 0001-Fix-avail_in-value-for-lz.deflate.patch
    - 0002-Lua5.2-require-does-not-set-a-global.patch

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 23 Aug 2013 22:30:08 +0200

lua-zlib (0.2+gitcfa1ecd-1) unstable; urgency=low

  * New snapshot supporting lua5.2 
  * Remove lua5.2 path, merged upstream
  * Package moved to git
  * Patch fixing upstream bug if no string is given to deflate
  * Patch making the test file lua5.2 compatible 
  * Fix copyright naming the license Expat and not MIT/X 

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 08 Apr 2012 18:34:50 +0200

lua-zlib (0.2-1) unstable; urgency=low

  * Initial release. (Closes: #668057)

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 08 Apr 2012 18:17:16 +0200
